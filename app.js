
//GAME CONTROLLER
let gameController = (function(){

    let Game = function(id, state, position){
        this.id = id;
        this.state = state;
        this.position = position;
    };

    let data = {
        allItems : {
            equis: [],
            check: []
        },
        lenghtArray : 3,
        contador: 0
    };

    //Declare principal Array
    let principalArray = new Array(data.lenghtArray);
    for(let x = 0; x < data.lenghtArray; x++){
        principalArray[x] = new Array(data.lenghtArray);
    }

    let calculateWin = function(type, id){
        let resultado = false;
        let position = id.substring(6, 7);
        let positionX = Math.floor(position/data.lenghtArray);
        let positionY = position%data.lenghtArray;

        principalArray[positionX][positionY] = type;
        //console.log(principalArray);
        resultado = calculateRows(type);
        if (!resultado) resultado = calculateColumns(type);
        if (!resultado) resultado = calculateDiagonals(type);
        return resultado;
    };

    let calculateRows = function(type){
        let result = false;
        for(let x = 0; x < data.lenghtArray; x++){
            for(let i = 0; i < data.lenghtArray; i++){
                if(principalArray[x][i] == type)  data.contador++;
            }
            result = data.contador == data.lenghtArray ? true : false;
            if(result) break;
            data.contador = 0;
        }
        return result;
    };

    let calculateColumns = function(type){
        let result = false;
        for(let x = 0; x < data.lenghtArray; x++){
            for(let i = 0; i < data.lenghtArray; i++){
                if(principalArray[i][x] == type)  data.contador++;
            }
            result = data.contador == data.lenghtArray ? true : false;
            if(result) break;
            data.contador = 0;
        }
        return result;
    };

    let calculateDiagonals = function(type){
        let result = false;
        let contador1 = 0;
        for(let x = 0; x < data.lenghtArray; x++){
            for(let i = 0; i < data.lenghtArray; i++){
                if(x == i)  {
                    if(principalArray[x][i] == type)  data.contador++;
                }
                if(x + i == data.lenghtArray - 1){
                    if(principalArray[x][i] == type)  contador1++;
                }
            }
        }
        result = data.contador == data.lenghtArray || contador1 == data.lenghtArray ? true : false;
        data.contador = 0;
        contador1 = 0;
        return result;
    };

    return {
        addItemValue: function(type, id){
            let position = id.substring(6, 7);
            let game = new Game(id, "1", position);
            data.allItems[type].push(game);
        },

        calculateWinner: function(type, id){
            return calculateWin(type, id);
        },

        initializeArray: function(){
            for(let x = 0; x < data.lenghtArray; x++){
                principalArray[x] = new Array(data.lenghtArray);
                for(let i = 0; i < data.lenghtArray; i++){
                    principalArray[x][i] = "0";
                }
            } 
        }
    }
})();

//UI CONTROLLER
let UIController = (function(){
    let DOMStrings = {
        buttonEquis : 'btnEquis',
        buttonCheck : 'btnCheck',
        card : 'card_0',
        buttonClear : 'btnClear'
    };

    let DOMNewStyles = {
        spanEquis : '<span class="glyphicon glyphicon-remove mark"></span>',
        spanCheck : '<span class="glyphicon glyphicon-ok mark"></span>'
    };

    return {
        getDOMstrings: function(){
            return DOMStrings;
        },

        getDOMstyles: function(){
            return DOMNewStyles;
        },

        addItemCard: function(id, itemSelected){
            document.getElementById(id).insertAdjacentHTML('beforeend', itemSelected);
        }
    }   
})();

//GLOBAL APP CONTROLLAR
let controller = (function(gameCtrl, UICtrl){

    let DOM = UICtrl.getDOMstrings();
    let NewStyles = UICtrl.getDOMstyles();
    let itemSelected = "";

    let setupEventListeners = function(){

        //console.log(DOM.buttonEquis); 
        document.getElementById(DOM.buttonEquis).addEventListener('click', funcitionFirstSelect);  
        document.getElementById(DOM.buttonCheck).addEventListener('click', funcitionFirstSelect);  
        document.getElementById(DOM.buttonClear).addEventListener('click', functionClearGame);     

        for(let i = 0; i < 9; i++){
            document.getElementById(DOM.card + i).addEventListener('click', selectCard);  
        }
    };

    let funcitionFirstSelect = function(){
        if (this.id == DOM.buttonEquis) itemSelected = NewStyles.spanEquis;
        else if (this.id == DOM.buttonCheck) itemSelected = NewStyles.spanCheck;
    };

    let selectCard = function(){
        //0. Validations
        if (itemSelected == "") {
            alert('Selec your first choice'); 
            return;
        }

        if(document.getElementById(this.id).childNodes.length > 0) return;

        //1. Add item
        UICtrl.addItemCard(this.id, itemSelected);

        //2. Add value
        let type = itemSelected == NewStyles.spanEquis ? "equis" : "check";
        gameCtrl.addItemValue(type, this.id);

        //3. Change item
        changeItemSelected(itemSelected);

        //4. Calculate if is winner
        if (gameCtrl.calculateWinner(type, this.id)) alert('you win');

    };

    let changeItemSelected = function(item){
        itemSelected = item == NewStyles.spanEquis ? NewStyles.spanCheck : NewStyles.spanEquis;
    };

    let functionClearGame = function(){
        for(let i = 0; i < 9; i++){
            document.getElementById(DOM.card + i).innerHTML = '';
        }
        gameCtrl.initializeArray();
        itemSelected = '';
    };

    return {
        init: function(){            
            console.log('Application has started');
            setupEventListeners();
            gameController.initializeArray();
        }
    };

})(gameController, UIController);

controller.init();